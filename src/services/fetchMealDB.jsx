export const getAllCuisines = async () => {
    const res = await fetch(
        "https://www.themealdb.com/api/json/v1/1/list.php?a=list"
      );
      const response = await res.json();
      console.log(response);
      return response.meals.map((area) => area.strArea);
};


export async function getMeals(cuisine) {
  const res = await fetch(
    `https://www.themealdb.com/api/json/v1/1/filter.php?a=${cuisine}`
  );
  const response = await res.json();
  return response;
}

export async function getMealsByIngredient(ing) {
  const res = await fetch(
    `https://www.themealdb.com/api/json/v1/1/filter.php?i=${ing}`
  );
  const response = await res.json();
  return response;
}


export async function getMealDetails(id) {  
  const res = await fetch(
    `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${id}`
  );
  return res.json();
}


export async function getMealIngredients(i) {  
  const res = await fetch(
    `https://www.themealdb.com/api/json/v1/1/list.php?i=${i}`
  );
  return res.json();
}