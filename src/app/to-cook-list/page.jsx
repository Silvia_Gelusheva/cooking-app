"use client";

import { useEffect, useState } from "react";

import Image from "next/image";
import Link from "next/link";
import { TiDelete } from "react-icons/ti";

export default function Favorites() {
  const [saved, setSaved] = useState([]);
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("favorites")) || [];
    setSaved(inputFromLocalStorage);
  }, []);

  const saveToLocalStorage = (items) => {
    localStorage.setItem("favorites", JSON.stringify(items));
  };

  const handleRemove = (item) => {
    const removeItem = saved.filter((s) => {
      return s.idMeal !== item.idMeal;
    });
    setSaved(removeItem);
    saveToLocalStorage(removeItem);
  };

  return (
    <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 mt-10">
      {saved?.map((favorite) => {
        return (
          <div
            key={favorite.idMeal}
            className="flex flex-col items-center rounded-md mb-6"
          >
            <Link href={`/cuisines/${favorite.strMeal}/${favorite.idMeal}`}>
              <Image
                src={favorite.strMealThumb}
                alt="image"
                width={200}
                height={200}
                className="rounded-xl"
              />
            </Link>
            <span className="flex flex-row w-full justify-center gap-6 pt-2 items-center">
              <p className="text-gray-700 font-semibold">{favorite.strMeal}</p>
              <button onClick={() => handleRemove(favorite)}>
                <TiDelete size={30} className="text-orange-500" />
              </button>
            </span>
          </div>
        );
      })}
    </div>
  );
}
