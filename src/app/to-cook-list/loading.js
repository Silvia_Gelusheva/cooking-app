import React from 'react'

export default function Loading() {
  return (
    <div>
      Loading to-cook list...
    </div>
  )
}
