"use client";

import "react-datepicker/dist/react-datepicker.css";

import * as Yup from "yup";

import { useEffect, useState } from "react";

import AddMenu from "./../components/AddMenu/page";
import MyMenus from "./../components/MyMenus/page";
import { arrayOf } from "prop-types";
import { v4 as uuidv4 } from "uuid";

export default function page() {
  const [toggle, setToggle] = useState(false);
  const [myMenus, setMyMenus] = useState([]);
  const options = {
    weekday: "short",
    year: "numeric",
    month: "short",
    day: "numeric",
  };
  const uniqueId = uuidv4();
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("myMenus")) || [];
    setMyMenus(inputFromLocalStorage);
  }, []);
  console.log(myMenus);
  useEffect(() => {
    if (typeof window !== "undefined") {
      localStorage.setItem("myMenus", JSON.stringify(myMenus));
    }
  }, [myMenus]);

  const initialValues = {
    date: new Date(),
    breakfast: "",
    lunch: "",
    dinner: "",
  };

  // const allDates = [];
  // for (const obj of myMenus) {
  //   allDates.push(obj.date)
  // }
  // const uniqueDates = new Set(allDates)
  // console.log(uniqueDates);
 

  const validationSchema = Yup.object().shape({    
    breakfast: Yup.string().required("Breakfast is required"),
    lunch: Yup.string().required("Lunch is required"),
    dinner: Yup.string().required("Dinner is required"),
  });

  const handleSubmit = (values) => {
    let newMenu = {
      id: uniqueId,
      date: values?.date.toLocaleDateString("en-us", options),
      breakfast: values?.breakfast,
      lunch: values?.lunch,
      dinner: values?.dinner,
    };
    setMyMenus([...myMenus, newMenu]);
    setToggle(!toggle);
  };

  const deleteMenu = (id) => {
    const removeItem = myMenus.filter((m) => {
      return m.id !== id;
    });
    setMyMenus(removeItem);
  };

  return (
    <>
      <div className="flex justify-center items-center">
        <div className="flex justify-start items-start w-full mx-4">
          <button
            className="btn btn-md w-full md:w-auto lg:w-auto hover:bg-gray-200 bg-inherit hover:text-white border-slate-400 text-slate-400 rounded-md"
            onClick={() => setToggle(!toggle)}
          >
            {!toggle ? "Add Menu" : "Cancel"}
          </button>
        </div>
      </div>
      {toggle ? (
        <AddMenu
          initialValues={initialValues}
          validationSchema={validationSchema}
          handleSubmit={handleSubmit}
        />
      ) : null}

      <MyMenus myMenus={myMenus} deleteMenu={deleteMenu} />
    </>
  );
}
