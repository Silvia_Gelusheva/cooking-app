"use client";

import { useEffect, useState } from "react";

import Image from "next/image";
import Link from "next/link";

export default function page({ params }) {
  const [myRecipes, setMyRecipes] = useState([]);
  const [current, setCurrent] = useState({});

  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("myRecipes")) || [];
    setMyRecipes(inputFromLocalStorage);
  }, []);

  useEffect(() => {
    try {
      let recipe = myRecipes.filter((r) => r.id == params.id);
      let currentRecipe = recipe[0];
      setCurrent({
        title: currentRecipe.title,
        url: currentRecipe.url,
        ing: currentRecipe.ing,
        content: currentRecipe.content,
      });
    } catch (error) {
      console.log(error.message);
    }
  }, [myRecipes]);

  return (
    <div className="bg-inherit rounded-2xl @container mb-4">
      <div className="pt-10 pl-10 pr-10 xl:flex gap-12">
        <div>
          <p className="font-bold text-3xl mb-2 text-orange-500">
            {" "}
            {current?.title}{" "}
          </p>
          <img
            src={
              current?.url ||
              "https://img.freepik.com/premium-vector/hand-drawn-recipe-book-template_23-2149297944.jpg?w=2000"
            }
            alt="Image"
            width={800}
            height={500}
            className="rounded-md 6xl:w-1/4 lg:py-4"
          />
          <div className="flex mt-2">
            <Link
              href={`/myrecipes`}
              className="px-4 py-2 text-white bg-orange-500 shadow-md rounded hover:bg-inherit hover:text-gray-800"
            >
              Back
            </Link>
          </div>
          <div className="tags mt-2">
            <p className="mb-1 font-semibold text-lg text-orange-400">
              Ingredients:
            </p>
            <ol>
              {current?.ing?.map((i) => {
                return (
                  <li>
                    <span className="inline-block mr-2">{i.name}</span>
                    <span className="inline-block font-semibold">
                      {i.quantity}
                    </span>
                  </li>
                );
              })}
            </ol>
          </div>
        </div>
        <div>
          <div className="tags mt-2 lg:mt-0">
            <span className="font-semibold text-lg text-orange-400">
              Directions:{" "}
            </span>
            <ol>
              {current?.content?.map((i, index) => {
                return (
                  <li>
                    <span className="inline-block mr-2 font-semibold text-gray-500">
                      STEP {index + 1}:{" "}
                    </span>
                    <span className="inline-block">{i.name}</span>
                  </li>
                );
              })}
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
}
