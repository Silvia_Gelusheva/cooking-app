import React from 'react'

export default function Loading() {
  return (
    <div>
      Loading my recipes...
    </div>
  )
}
