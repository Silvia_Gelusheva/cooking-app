"use client";

import * as Yup from "yup";

import { useEffect, useMemo, useState } from "react";

import AddRecipe from "../components/AddRecipe/page";
import MyRecipes from "../components/MyRecipes/page";
import SearchBar from "../components/SearchBar/page";
import { v4 as uuidv4 } from "uuid";

export default function page() {
  const [toggle, setToggle] = useState(false);
  const [myRecipes, setMyRecipes] = useState([]);
  const [query, setQuery] = useState("");
  const uniqueId = uuidv4();
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("myRecipes")) || [];
    setMyRecipes(inputFromLocalStorage);
  }, []);
  console.log(myRecipes);
  useEffect(() => {
    if (typeof window !== "undefined") {
      localStorage.setItem("myRecipes", JSON.stringify(myRecipes));
    }
  }, [myRecipes]);

  const initialValues = {
    title: "",
    url: "",
    ing: [{ name: "", quantity: "" }],
    content: [{ name: "" }],
  };
  const onSubmit = (values) => {
    handleSubmit(values);
  };

  const isValidUrl = (url) => {
    if (url !== undefined) {
      try {
        new URL(url);
      } catch (e) {
        return false;
      }
    }
    return true;
  };
  const validationSchema = Yup.object({
    title: Yup.string().required("Title is required!"),
    url: Yup.string().test("is-url-valid", "URL is not valid", (value) =>
      isValidUrl(value)
    ),
    // ing: Yup.string().required("Ingredients are required!"),
    // content: Yup.string().required("Instructions are required!"),
  });

  const handleSubmit = (values) => {
    let newRecipe = {
      id: uniqueId,
      title: values?.title,
      url: values?.url,
      ing: values?.ing,
      content: values?.content,
    };
    const updatedData = [...myRecipes, newRecipe];
    setMyRecipes([...myRecipes, newRecipe]);

    setToggle(!toggle);
  };

  const filteredRecipes = useMemo(
    () =>
      myRecipes.filter((rec) => {
        return rec.title.toLowerCase().includes(query.toLowerCase());
        //  ||
        // rec.content.toLowerCase().includes(query.toLowerCase())
        //  ||
        // rec.ing.toLowerCase().includes(query.toLowerCase())
      }),
    [myRecipes, query]
  );

  const deleteMyRecipe = (id) => {
    console.log(myRecipes);
    const removeItem = myRecipes.filter((r) => {
      return r.id !== id;
    });
    setMyRecipes(removeItem);
  };

  return (
    <div>
      <div className="flex flex-col lg:flex-row justify-start items-start gap-4 m-4">
        <button
          className="w-full lg:w-auto btn btn-md hover:bg-gray-200 bg-inherit hover:text-white border-slate-400 text-slate-400 rounded-md"
          onClick={() => setToggle(!toggle)}
        >
          {!toggle ? "Add Recipe" : "Cancel"}
        </button>
        <SearchBar query={query} setQuery={setQuery} />
      </div>
      {toggle ? (
        <AddRecipe
          onSubmit={onSubmit}
          initialValues={initialValues}
          validationSchema={validationSchema}
        />
      ) : null}

      <MyRecipes myRecipes={filteredRecipes} deleteMyRecipe={deleteMyRecipe} />
    </div>
  );
}
