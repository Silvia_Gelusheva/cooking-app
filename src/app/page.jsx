import Image from "next/image";
import image from "../../public/20096.jpg";

export default function Home() {
  return (
    <div className="flex flex-col justify-center items-center">
      <div className="flex items-center justify-center mt-6">
        <Image
          src={image}
          alt="image"
          width={400}
          height={300}
          className="rounded-t-lg"
        />
      </div>

      <div className="flex flex-col justify-end items-end text-gray-800 lg:hidden mb-6">
        <h1 className="text-3xl font-bold ">WHAT TO COOK </h1>
        <p className="italic font-bold">...today?</p>
      </div>
    </div>
  );
}
