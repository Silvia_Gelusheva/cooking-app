import Legend from "../components/Legend/page";

export default function About() {
  return (
    <div className="flex items-center justify-center mt-4">
      <div className="w-full rounded overflow-hidden">
        <div className="px-6 py-4">
          <p className="text-gray-700 italic text-center">
            What should I cook for dinner tonight? That forever question we have
            every single day, when contemplating what to eat today. This app
            compiled list of easy ideas for every day and also a shopping list,
            to be sure you have all the ingredients needed. You also have the
            option to save your own recipes and to plan your daily menu.
          </p>
        </div>
        <Legend />
      </div>
     
    </div>
  );
}
