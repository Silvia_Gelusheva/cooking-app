import Link from "next/link";
import SearchBar from "../components/SearchBar/page";
import { getAllCuisines } from "@/services/fetchMealDB";

export default async function Cuisines() {
  const cuisines = await getAllCuisines();

  return (
    <div className="my-6">
      <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 m-6">
        {cuisines?.map((cuisine, idx) => {
          return (
            <Link className="" href={`/cuisines/${cuisine}`}>
              <div
                key={idx}
                className="text-slate-500 text-xl bg-orange-100 hover:bg-inherit font-semibold shadow-md rounded text-center py-8"
              >
                {cuisine}
              </div>
            </Link>
          );
        })}
      </div>
    </div>
  );
}
