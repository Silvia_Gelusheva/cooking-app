import React from 'react'

export default function Loading() {
  return (
    <div className='flex justify-center items-center text-xl mt-72'>
     <p> Loading meals...</p>
    </div>
  )
}