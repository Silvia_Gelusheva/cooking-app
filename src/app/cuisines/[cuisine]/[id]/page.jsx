import { AiFillYoutube } from "react-icons/ai";
import Image from "next/image";
import Input from "@/app/components/input/page";
import Link from "next/link";
import { getMealDetails } from "@/services/fetchMealDB";
export default async function page({ params }) {
  const mealDetails = await getMealDetails(params.id);

  const details = mealDetails.meals[0];
  // console.log(details);

  const ingredients = Object.keys(details)

    .filter((key) => key.indexOf("Ingredient") > 0)
    .map((ingKey) => (details[ingKey]?.length ? details[ingKey] : undefined))
    .filter(Boolean);
  // console.log(ingredients);

  const measures = Object.keys(details)
    .filter((key) => key.indexOf("Measure") > 0)
    .map((mesKey) => (details[mesKey]?.length ? details[mesKey] : undefined))
    .filter(Boolean);

  const ing = ingredients.map((tag, i) => (
    <div key={i} className="flex gap-3">
      <Input tag={tag} meal={details.strMeal} /> {tag}
    </div>
  ));

  const mes = measures.map((tag, i) => (
    <h2 key={i} className="">
      {tag}
    </h2>
  ));
  console.log(mes);
  return (
    <div className="bg-inherit rounded-2xl @container">
      <div className="px-10 mb-6 xl:flex gap-4">
        <div className="flex flex-col">
          <p className="font-bold text-3xl mb-4 text-slate-500">
            {details.strMeal}
          </p>
          <Image
            src={details.strMealThumb}
            alt="Image"
            width={800}
            height={500}
            className="rounded-md @6xl:w-1/4  lg:py-4"
          />

          <div className="flex my-4">
            <Link
              href={`/cuisines/${details.strArea}`}
              className="px-4 py-2 text-white font-semibold bg-orange-500 shadow-md rounded hover:bg-inherit hover:text-gray-800"
            >
              Back
            </Link>
          </div>
        </div>
        <div>
          <div className="tags">
            <span className="font-bold text-lg text-slate-500 ">
              Directions:
            </span>
            <article>{details.strInstructions}</article>
          </div>
          <div className="tags mt-6 flex flex-row items-center gap-2">
            <span>
              <AiFillYoutube size={28} className="text-red-600" />{" "}
            </span>
            <a
              className="text-blue-500 italic text-sm lg:text-base"
              target="_blank"
              href={details.strYoutube}
              rel="noreferrer"
            >
              Watch on Youtube: {details.strMeal}
            </a>
          </div>
          <div className="tags mt-3">
            <p className="mb-1 font-bold text-lg text-slate-500">
              Ingredients:
            </p>

            <div className="flex gap-4 text-md">
              <span className="inline-block">{ing}</span>
              <span className="inline-block">{mes}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
