"use client";

import { Card, Row, Text } from "@nextui-org/react";
import { PiCookingPotFill, PiCookingPotLight } from "react-icons/pi";
import { useEffect, useState } from "react";

import Image from "next/image";
import Link from "next/link";
import React from "react";
import { getMeals } from "@/services/fetchMealDB";

export default function Page({ params }) {
  const [cuisine, setCuisine] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    getMeals(params.cuisine).then(setCuisine);
  }, []);
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("favorites")) || [];
    setFavorites(inputFromLocalStorage);
  }, []);

  const saveToLocalStorage = (items) => {
    if (typeof window !== "undefined") {
      localStorage.setItem("favorites", JSON.stringify(items));
    }
  };

  const addFavorite = (item) => {
    const newFavorite = [...favorites, item];

    saveToLocalStorage(newFavorite);
    setFavorites(newFavorite);
  };

  const handleRemove = (item) => {
    const removeItem = favorites.filter((s) => {
      return s.idMeal !== item.idMeal;
    });
    setFavorites(removeItem);
    saveToLocalStorage(removeItem);
  };

  return (
    <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mx-4 mt-10 mb-4">
      {cuisine?.meals?.map((meal) => {
        return (
          <Card
            isPressable
            key={meal.idMeal}
            css={{ backgroundColor: "inherit" }}
          >
            <Link href={`/cuisines/${meal.strMeal}/${meal.idMeal}`}>
              <Card.Body>
                <Card.Image
                  src={meal.strMealThumb}
                  alt="image"
                  objectFit="cover"
                  width="100%"
                  height={200}
                  className="rounded-xl"
                />
              </Card.Body>
            </Link>
            <Card.Footer css={{ justifyItems: "flex-start" }}>
              <Row wrap="wrap" justify="space-between" align="center">
                <Text className="text-slate-500 font-semibold ">
                  {meal.strMeal}
                </Text>

                {!favorites.map((m) => m.idMeal).includes(meal.idMeal) ? (
                  <button onClick={() => addFavorite(meal)}>
                    <span className="flex items-center gap-1">
                      <p className="text-sm">to</p>
                      <PiCookingPotLight size={28} />
                    </span>
                  </button>
                ) : (
                  <button onClick={() => handleRemove(meal)}>
                    <span className="flex items-center gap-1">
                      <p className="text-sm text-orange-500">will</p>
                      <PiCookingPotFill size={28} className="text-orange-500" />
                    </span>
                  </button>
                )}
              </Row>
            </Card.Footer>
          </Card>
        );
      })}
    </div>
  );
}
