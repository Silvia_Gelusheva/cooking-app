"use client";

import { useEffect, useState } from "react";

import { BsCartCheck } from "react-icons/bs";
import { IoMdAddCircle } from "react-icons/io";

export default function Input({ tag, meal }) {
  const [saved, setSaved] = useState([]);
  const recipe = meal;
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("ingredients")) || [];
    setSaved(inputFromLocalStorage);
  }, []);

  const saveToLocalStorage = (item, meal) => {
    const newItem = {
      recipe: meal,
      item,
      complete: false,
      id: Math.random(),
    };
    const arr = JSON.parse(localStorage.getItem("ingredients")) || [];
    arr.push(newItem);
    localStorage.setItem("ingredients", JSON.stringify(arr));
    setSaved(arr);
  };

  const handleRemove = (item) => {
    const removeRecipeItem = saved.filter((p) => {
      return p.item !== item;
    });
    setSaved(removeRecipeItem);
    localStorage.setItem("ingredients", JSON.stringify(removeRecipeItem));
  };
  // console.log()
  return (
    <div className="flex items-center justify-center">
      {saved?.map((s) => s.item).includes(tag) ? (
        <button onClick={() => handleRemove(tag)}>
          <BsCartCheck color="red" size={20} />
        </button>
      ) : (
        <button onClick={() => saveToLocalStorage(tag, meal)}>
          <IoMdAddCircle color="green" size={20} />
        </button>
      )}
    </div>
  );
}
