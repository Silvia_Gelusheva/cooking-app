"use client";

import { ErrorMessage, Field, FieldArray } from "formik";

import { RiAddCircleFill } from "react-icons/ri";
export default function AddDirections({ values }) {
  return (
    <FieldArray name="content">
      {({ push, remove }) => (
        <div className="border-2 border-white lg:border-slate-200 rounded mt-4 p-3">
          <p className="text-slate-400 flex justify-center">Directions</p>
          {values?.content?.map((c, index) => (
            <div
              key={index}
              className="flex flex-row justify-between items-center gap-2 my-3"
            >
              <span className="text-sm text-slate-500">Step {index + 1}</span>
              <div className="border">
                <Field
                  type="text"
                  id={`content[${index}].name`}
                  name={`content[${index}].name`}
                  as="textarea"
                  placeholder="directions"
                  className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                />
                <ErrorMessage name={`content[${index}].name`} component="div" />
              </div>
              <button
                type="button"
                onClick={() => remove(index)}
                className="btn btn-md bg-white text-slate-400"
              >
                X
              </button>
            </div>
          ))}
          <div className="flex justify-end items-end"></div>
          <div className="flex flex-row justify-end">
            <RiAddCircleFill
              size={24}
              className="text-slate-400"
              onClick={() => push({ name: "" })}
            />
          </div>
        </div>
      )}
    </FieldArray>
  );
}
