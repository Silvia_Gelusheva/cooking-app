"use client"

import { ErrorMessage, Field, FieldArray } from "formik";

import { RiAddCircleFill } from "react-icons/ri";
export default function AddIngredients({values}) {
  return (
    <FieldArray name="ing">
                    {({ push, remove }) => (
                      <div className="border-2 border-white lg:border-slate-200 rounded mt-4 p-3">
                        <p className="text-slate-400 flex justify-center">
                          Ingredients
                        </p>
                        {values?.ing?.map((_, index) => (
                          <div
                            key={index}
                            className="flex flex-row justify-between items-center gap-2 my-3"
                          >
                            <div className="border">
                              <Field
                                type="text"
                                id={`ing[${index}].name`}
                                name={`ing[${index}].name`}
                                placeholder="name"
                                className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                              />
                              <ErrorMessage
                                name={`ing[${index}].name`}
                                component="div"
                              />
                            </div>
                            <div>
                              <Field
                                type="text"
                                id={`ing[${index}].quantity`}
                                name={`ing[${index}].quantity`}
                                placeholder="quantity"
                                className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                              />
                              <ErrorMessage
                                name={`ing[${index}].quantity`}
                                component="div"
                              />
                            </div>
                            <button
                              type="button"
                              onClick={() => remove(index)}
                              className="btn btn-md bg-white text-slate-400"
                            >
                              X
                            </button>
                          </div>
                        ))}
                        <div className="flex justify-end items-end"></div>
                        <div className="flex flex-row justify-end">
                          <RiAddCircleFill
                            size={24}
                            className="text-slate-400"
                            onClick={() => push({ name: "", quantity: "" })}
                          />
                        </div>
                      </div>
                    )}
                  </FieldArray>
  )
}
