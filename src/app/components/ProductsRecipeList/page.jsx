"use client";

import { Checkbox } from "@nextui-org/react";
import { TiDelete } from "react-icons/ti";

export default function page({
  saved,
  recipeTitle,
  handleCheckboxRecipe,
  deleteProductFromRecipe,
  deleteAllRecipeProductsList,
}) {
  return (
    <>
      <div className="bg-gray-100 rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">
        <div className="flex justify-between items-center pb-4">
          <h1 className="text-gray-800 text-center uppercase font-bold flex">
            {recipeTitle}
          </h1>
          <button
            className="flex-no-shrink p-2 border-2 rounded border-teal bg-gray-400 hover:bg-orange-500 text-white"
            onClick={() => deleteAllRecipeProductsList(recipeTitle)}
          >
            DEL
          </button>
        </div>
        {saved?.map((product) => {
          if (product.recipe === recipeTitle) {
            return (
              <div key={product.id} className="flex mb-4 items-center">
                <Checkbox
                  aria-label="checkbox"
                  className="w-8 h-8 mr-4"
                  onChange={() => handleCheckboxRecipe(product.id)}
                />
                <p
                  className={
                    !product?.complete
                      ? "w-full text-gray-800 font-semibold"
                      : "w-full text-gray-400 italic line-through"
                  }
                >
                  {product.item}
                </p>
                <button onClick={() => deleteProductFromRecipe(product.id)}>
                  <TiDelete size={30} className="text-gray-400 hover:text-orange-500" />
                </button>
              </div>
            );
          }
        })}
      </div>
    </>
  );
}
