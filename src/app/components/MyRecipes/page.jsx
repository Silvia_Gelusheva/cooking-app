"use client"

import { Card, Link, Row, Text } from "@nextui-org/react";

import { AiTwotoneDelete } from "react-icons/ai";

export default function MyRecipes({ myRecipes, deleteMyRecipe }) {
  return (
    <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mx-4 mt-10">
      {myRecipes?.map((rec) => {
        return (
          <Card key={rec.id} isPressable>
            <Card.Body css={{ p: 0 }}>
              <Card.Image
                src={
                  rec.url ||
                  "https://img.freepik.com/premium-vector/hand-drawn-recipe-book-template_23-2149297944.jpg?w=2000"
                }
                objectFit="cover"
                width="100%"
                height={200}
                alt="title"
                className="cursor-auto"
              />
            </Card.Body>

            <Card.Footer css={{ justifyItems: "flex-start" }}>
              <Row wrap="wrap" justify="space-between" align="center">
                <Link href={`/myrecipes/${rec.id}`}>
                  <Text b className="hover:text-orange-500">
                    {rec.title}
                  </Text>
                </Link>
                <Text
                  css={{
                    color: "$accents7",
                    fontWeight: "$semibold",
                    fontSize: "$sm",
                  }}
                ></Text>
                <AiTwotoneDelete
                  size={20}
                  className="hover:text-orange-500"
                  onClick={() => deleteMyRecipe(rec.id)}
                />
              </Row>
            </Card.Footer>
          </Card>
        );
      })}
    </div>
  );
}
