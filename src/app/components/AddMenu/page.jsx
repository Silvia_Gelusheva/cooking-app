"use client"

import { ErrorMessage, Field, Form, Formik } from "formik";

import DatePicker from "react-datepicker";

export default function page({
  initialValues,
  validationSchema,
  handleSubmit,
}) {
  return (
    <div className="max-w-2xl bg-white mb-4 px-5 m-auto w-full">
      <div className="flex justify-center lg:bg-inherit bg-orange-100 items-center rounded-lg mt-4 lg:mt-0">
        <div className="w-full px-6 py-8">
          <div className="text-3xl mb-6 font-semibold text-center text-orange-300">
            Daily Menu
          </div>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {({ values, handleChange, setFieldValue, isSubmitting }) => (
              <Form>
                <div className="mb-4 flex flex-row">
                  <label className="text-gray-500 mr-2">Date:</label>

                  <DatePicker
                    selected={values?.date}
                    onChange={(date) => setFieldValue("date", date)}
                    className="text-orange-300 font-semibold cursor-pointer bg-inherit"
                  />
                </div>
                <div className="flex flex-col gap-4">
                  <div>
                    <Field
                      type="text"
                      name="breakfast"
                      placeholder="Breakfast"
                      className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                    />
                    <ErrorMessage
                      name="breakfast"
                      component="div"
                      className="text-orange-500"
                    />
                  </div>
                  <div>
                    <Field
                      type="text"
                      name="lunch"
                      placeholder="lunch"
                      className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                    />
                    <ErrorMessage
                      name="lunch"
                      component="div"
                      className="text-orange-500"
                    />
                  </div>
                  <div>
                    <Field
                      type="text"
                      name="dinner"
                      placeholder="Dinner"
                      className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                    />
                    <ErrorMessage
                      name="dinner"
                      component="div"
                      className="text-orange-500"
                    />
                  </div>
                </div>
                <button
                  disabled={isSubmitting}
                  className="w-full mt-4 btn btn-md hover:bg-gray-200 bg-inherit hover:text-white border-slate-400 text-slate-400 rounded-md"
                  type="submit"
                >
                  Submit
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
