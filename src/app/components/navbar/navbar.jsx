import { FaRegQuestionCircle, FaShoppingCart } from "react-icons/fa";
import { GiHearts, GiMeal } from "react-icons/gi";

import { BiSolidBookBookmark } from "react-icons/bi";
import { BsCalendar2DateFill } from "react-icons/bs";
import Link from "next/link";
import { LuChefHat } from "react-icons/lu";

export default function Navbar() {
  const navBarLinks =
    "hover:text-orange-500 hover:underline decoration-orange-500 decoration-2 underline-offset-8 focus:underline";

  return (
    <>
      <div className="flex flex-row gap-20 justify-center lg:justify-between items-center mb-10 w-full bg-inherit text-gray-800  p-6 shadow-md">
        <div className="flex">
          <Link href="/" className="flex flex-col justify-center items-center hover:text-orange-500">
            <LuChefHat size={24} className="hidden lg:inline" />
            <p className="text-sm font-semibold">Home</p>
          </Link>
        </div>

        <div className="hidden lg:flex">
          <span className="flex flex-row">
            <h1 className="text-lg font-semibold text-gray-800 mr-2">
              What to cook
            </h1>{" "}
            <p className="italic text-lg text-gray-800">...today?</p>
          </span>
        </div>

        <div className="flex flex-row gap-6">
          <Link
            href="/cuisines"
            className="flex flex-col justify-center items-center hover:text-orange-500"
          >
            <GiMeal size={24} className="hidden lg:inline" />
            <p className="text-sm font-semibold">Cuisines</p>
          </Link>
          <Link
            href="/to-cook-list"
            className="flex flex-col justify-center items-center hover:text-orange-500"
          >
            <GiHearts size={24} className="hidden lg:inline" />
            <p className="text-sm font-semibold">Favorites</p>
          </Link>      
          <Link
            href="/about"
            className="flex flex-col justify-center items-center hover:text-orange-500"
          >
            <FaRegQuestionCircle size={24} className="hidden lg:inline" />
            <p className="text-sm font-semibold">About</p>
          </Link>
        </div>
      </div>

      <div className="flex flex-row justify-center gap-4 mb-4">
        <Link
          href="/daily-menu"
          className="btn btn-lg text-gray-800 bg-white flex justify-between border-slate-200 border-2 focus:bg-gray-800 focus:text-white"
        >
          <BsCalendar2DateFill size={36} />
          <p className="hidden lg:inline">My daily meals</p>
        </Link>
        <Link
          href="/myrecipes"
          className="btn btn-lg text-yellow-700 bg-white flex justify-between border-slate-200 border-2 focus:bg-yellow-900 focus:text-white"
        >
          <BiSolidBookBookmark size={36} />
          <p className="hidden lg:inline">My recipes</p>
        </Link>
        <Link
          href="/to-buy-list"
          className="btn btn-lg text-orange-500  bg-white flex justify-between border-slate-200 border-2 focus:bg-orange-500 focus:text-white"
        >
          <FaShoppingCart size={36} />
          <p className="hidden lg:inline">My Shopping List</p>
        </Link>
      </div>
    </>
  );
}
