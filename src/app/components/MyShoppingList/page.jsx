"use client";

import { Checkbox } from "@nextui-org/react";
import React from "react";
import { TiDelete } from "react-icons/ti";

export default function page({
  input,
  setInput,
  list,
  handleCheckboxChange,
  addProduct,
  deleteProduct,
}) {
  return (
    <>
      <div className="bg-orange-100 rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">
        <div className="mb-4">
          <h1 className="text-gray-500 text-center uppercase font-bold">
            My Shopping List
          </h1>
          <div className="flex mt-4">
            <input
              className="shadow appearance-none bg-white border rounded w-full py-2 px-3 mr-4 text-grey-800"
              placeholder="Add Product"
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
            <button
              onClick={addProduct}
              className="flex-no-shrink p-2 border-2 rounded border-teal bg-gray-400 hover:bg-orange-500 text-white"
            >
              Add
            </button>
          </div>
        </div>
        <div>
          {list?.map((product) => {
            return (
              <div key={product.id} className="flex mb-4 items-center">
                <Checkbox
                  type="checkbox"
                  id=""
                  aria-label="checkbox"
                  className="w-8 h-8 mr-4"
                  checked={product?.complete ? "checked" : ""}
                  onChange={() => handleCheckboxChange(product.id)}
                />
                <p
                  className={
                    !product?.complete
                      ? "w-full text-gray-800 font-semibold"
                      : "w-full text-gray-400 italic line-through"
                  }
                >
                  {product.title}
                </p>
                <button onClick={() => deleteProduct(product.id)}>
                  <TiDelete size={30} className="text-gray-400  hover:text-orange-500" />
                </button>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}
