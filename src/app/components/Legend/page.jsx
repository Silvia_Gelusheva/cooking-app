import { GiHearts, GiMeal } from "react-icons/gi";

import { BiSolidBookBookmark } from "react-icons/bi";
import { BsCalendar2DateFill } from "react-icons/bs";
import { FaShoppingCart } from "react-icons/fa";
import { LuChefHat } from "react-icons/lu";

export default function page() {
  return (
    <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mx-4 mt-4">
      <div className="p-6 md:p-10 rounded-xl bg-blue-50">
        <div class="inline-flex rounded-full bg-white p-4">
          <GiMeal size={28} className="text-slate-400" />
        </div>

        <h3 className="mt-4 text-base md:text-xl font-medium text-gray-800">
          Cuisines
          <br />
          Travel around the world without leaving your kitchen with these
          international recipes
        </h3>
        <p className="mt-4 text-sm text-gray-600">
          Browse our selection of world food recipes and you'll find cuisines
          divided up by country or region to make it simple to select what
          you're going to cook!
        </p>
      </div>

      <div className="p-6 md:p-10 rounded-xl bg-blue-50">
        <div class="inline-flex rounded-full bg-white p-4">
          <GiHearts size={28} className="text-slate-400" />
        </div>

        <h3 className="mt-4 text-base md:text-xl font-medium text-gray-800">
          Favorite meals
          <br />
          Click the pot to save any recipes to your favorites folder!
        </h3>
        <p className="mt-4 text-sm text-gray-600">
          You may save your no.1 recipes and refer them regularly.
        </p>
      </div>

      <div className="p-6 md:p-10 rounded-xl bg-blue-50">
        <div class="inline-flex rounded-full bg-white p-4">
          <FaShoppingCart size={28} className="text-slate-400" />
        </div>

        <h3 className="mt-4 text-base md:text-xl font-medium text-gray-800">
          My Shopping List
          <br />
          Plan your daily or weekly menus
        </h3>
        <p className="mt-4 text-sm text-gray-600">
          This app gives you the opportunity to easily organize your shopping
          lists. It is minimalistic, intuitive and easy to use, it keeps your
          grocery shopping list without bothering you with complex features.
        </p>
      </div>

      <div className="p-6 md:p-10 rounded-xl bg-blue-50">
        <div class="inline-flex rounded-full bg-white p-4">
          <BsCalendar2DateFill size={28} className="text-slate-400" />
        </div>

        <h3 className="mt-4 text-base md:text-xl font-medium text-gray-800">
          My Daily Meals
          <br />
          Plan your daily or weekly menus
        </h3>
        <p className="mt-4 text-sm text-gray-600">
          Instead of eating meals in huge chunks, think about the benefits of
          maintaining your energy levels at a consistent rate across the day. By
          scheduling your meals, and building a healthy diet, you can maximize
          your digestive health while preventing the development of
          cardiovascular disease, type 2 diabetes, and obesity.
        </p>
      </div>

      <div className="p-6 md:p-10 rounded-xl bg-blue-50">
        <div class="inline-flex rounded-full bg-white p-4">
          <BiSolidBookBookmark size={28} className="text-slate-400" />
        </div>

        <h3 className="mt-4 text-base md:text-xl font-medium text-gray-800">
          My Recipes
          <br />
          Plan your daily or weekly menus
        </h3>
        <p className="mt-4 text-sm text-gray-600">
         Use your personal digital recipe book on your device. Store your favorite
          recipes in the app, always keep them close by so you know what
          products you need when shopping. Easily find recipes, follow them,
          recalculate ingredients to change the yield.
        </p>
      </div>
    </div>
  );
}
