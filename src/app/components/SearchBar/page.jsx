"use client"
export default function SearchBar({query, setQuery}) {
  return (
    <div className="text-white w-full lg:w-auto">
      <input
        type="search"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        className=" border border-slate-400 w-full p-3 bg-inherit text-gray-500 placeholder:text-slate-400 rounded-md"
        placeholder="Search recipe..."
      />     
    </div>
  );
}
