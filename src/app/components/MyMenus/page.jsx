"use client"

import { TiDelete } from "react-icons/ti";

export default function page({ myMenus, deleteMenu }) {
  return (
    <div className="grid xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mx-4 mt-4">
      {myMenus
        ?.sort((a, b) => Number(new Date(a.date)) - Number(new Date(b.date)))
        ?.map((m) => {
          return (
            <div key={m?.id}>
              <div className="p-6 md:p-10 rounded-xl bg-orange-100">
                <div className="">
                  <div className="flex w-full justify-between items-center rounded-full bg-white p-4 font-semibold text-lg text-gray-500">
                    {m?.date}
                    <button>
                      <TiDelete
                        size={28}
                        onClick={() => deleteMenu(m?.id)}
                        className="hover:text-orange-500 text-slate-400"
                      />
                    </button>
                  </div>
                </div>
                <span className="flex flex-row gap-2 items-end">
                  <p className="text-slate-400 mt-2  italic">breakfast:</p>
                  <h3 className="mt-2 text-gray-600 ">{m?.breakfast}</h3>
                </span>
                <span className="flex flex-row gap-2 items-end">
                  <p className="text-slate-400  mt-2  italic">lunch:</p>
                  <h3 className="mt-2 text-gray-600 ">{m?.lunch}</h3>
                </span>
                <span className="flex flex-row gap-2 items-end">
                  <p className="text-slate-400  mt-2  italic">dinner:</p>
                  <h3 className="mt-2  text-gray-600 ">{m?.dinner}</h3>
                </span>
              </div>
            </div>
          );
        })}
    </div>
  );
}
