"use client";

import { ErrorMessage, Field, Form, Formik } from "formik";

import AddDirections from "../AddDirections/page";
import AddIngredients from "../AddIngredients/page";

export default function AddRecipe({
  initialValues,
  onSubmit,
  validationSchema,
}) {
  return (
    <div className="max-w-2xl bg-white mb-4 px-5 m-auto w-fulls">
      <div className="flex justify-center lg:bg-inherit bg-gray-200 items-center rounded-lg mt-4 lg:mt-0">
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {({ values }) => (
            <Form>
              <div className="md:flex">
                <div className="w-full px-6 py-8">
                  <div className="text-3xl mb-6 font-semibold text-center text-gray-400">
                    New Recipe
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="title"
                      placeholder="name"
                      className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="title"
                      component="span"
                      className="text-sm font-bold text-error"
                    />
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="url"
                      placeholder="imageUrl"
                      className="border-solid border-gray-200 border-2 rounded-lg p-3 md:text-xl w-full"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="url"
                      component="span"
                      className="text-sm font-bold text-error"
                    />
                  </div>

                  <AddIngredients values={values} />                

                  <AddDirections values={values}/>

                  <div className="flex justify-end mt-4">
                    <button
                      className="w-full mt-4 btn btn-md hover:bg-gray-200 bg-inherit hover:text-white border-slate-400 text-slate-400 rounded-md"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
