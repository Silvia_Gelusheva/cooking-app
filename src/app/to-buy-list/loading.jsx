import React from 'react'

export default function Loading() {
  return (
    <div>
      Loading to-buy list...
    </div>
  )
}
