"use client";

import { useEffect, useState } from "react";

import MyShoppingList from "../components/MyShoppingList/page";
import ProductsRecipeList from "../components/ProductsRecipeList/page";
import { v4 as uuidv4 } from 'uuid';

export default function List() {
  const [list, setList] = useState([]);
  const [saved, setSaved] = useState([]);
  const [input, setInput] = useState("");
  const uniqueId = uuidv4();
  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("list")) || [];
    setList(inputFromLocalStorage);
  }, []);

  useEffect(() => {
    const inputFromLocalStorage =
      JSON.parse(localStorage.getItem("ingredients")) || [];
    setSaved(inputFromLocalStorage);
  }, []);

  useEffect(() => {
    if (typeof window !== "undefined") {
      localStorage.setItem("list", JSON.stringify(list));
    }
  }, [list]);

  const addProduct = (e) => {
    e.preventDefault();
    let newProduct = {
      id: uniqueId,
      title: input,
      complete: false,
    };
    setList([...list, newProduct]);
    setInput("");
  };

  const deleteProduct = (id) => {
    const removeItem = list.filter((p) => {
      return p.id !== id;
    });
    setList(removeItem);
  };

  const deleteProductFromRecipe = (id) => {
    const removeRecipeItem = saved.filter((p) => {
      return p.id !== id;
    });
    setSaved(removeRecipeItem);
    localStorage.setItem("ingredients", JSON.stringify(removeRecipeItem));
  };

  const handleCheckboxChange = (id) => {
    const newListOfProducts = list.map((product) => {
      if (product.id === id) {
        return { ...product, complete: !product.complete };
      } else {
        return product;
      }
    });
    setList(newListOfProducts);
    // console.log(newListOfProducts);
  };

  const handleCheckboxRecipe = (id) => {
    const newListOfRecipeProducts = saved.map((product) => {
      if (product.id === id) {
        console.log(product);
        return { ...product, complete: !product.complete };
      } else {
        return product;
      }
    });
    setSaved(newListOfRecipeProducts);
    localStorage.setItem(
      "ingredients",
      JSON.stringify(newListOfRecipeProducts)
    );
  };

  const deleteAllRecipeProductsList = (recipeTitle) => {
    const removeRecipeItem = saved.filter((p) => {
      return p.recipe !== recipeTitle;
    });
    setSaved(removeRecipeItem);
    localStorage.setItem("ingredients", JSON.stringify(removeRecipeItem));
  };

  const title = saved.map((s) => s.recipe);
  // console.log(title);

  const uniqueTitles = new Set(title);
  // console.log(uniqueTitles);

  //console.log(saved);
  return (
    <div className="h-100 w-full flex flex-col items-center justify-center bg-teal-lightest font-sans p-4">
      <MyShoppingList
        input={input}
        setInput={setInput}
        list={list}
        handleCheckboxChange={handleCheckboxChange}
        addProduct={addProduct}
        deleteProduct={deleteProduct}
      />

      {[...uniqueTitles].map((recipeTitle, i) => (
        <ProductsRecipeList
          key={i}
          recipeTitle={recipeTitle}
          saved={saved}
          handleCheckboxRecipe={handleCheckboxRecipe}
          deleteProductFromRecipe={deleteProductFromRecipe}
          deleteAllRecipeProductsList={deleteAllRecipeProductsList}
        />
      ))}
    </div>
  );
}
