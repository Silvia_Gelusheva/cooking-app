# what-to-cook-app
What should I cook for dinner tonight? That forever question we have every single day, when contemplating what to eat today. This *app* compiled list of easy ideas for every day and also a shopping list, to be sure you have all the ingredients needed. All of these recipes make great daily meals, and there’s something for every taste. Craving pasta? Stirfry? Tacos?  No problem! So go through this meal-ideas list, and be sure to try our recipes, because it’s going to be your new go-to whenever you’re wondering *what-to-cook*!
        
### Technologies:

- Next.js
- JavaScript
- TailwindCSS
- Next UI
- Formik


### Setup:

Open the root directory 'cooking-app'. Install all packages by running: *`npm install`*
Then run: *`npm run dev`*


#### **What to cook**

<img src='readme-img/homepage.png' width='300px'> <img src='readme-img/cuisines.png' width='300px'> <img src='readme-img/cuisine.png' width='300px'> <img src='readme-img/recipe.png' width='300px'> <img src='readme-img/fav.png' width='300px'> <img src='readme-img/shopping-lists.png' width='300px'> <img src='readme-img/add-meal.png' width='300px'> <img src='readme-img/add-recipe.png' width='300px'> <img src='readme-img/about.png' width='300px'>
